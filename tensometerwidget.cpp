#include "tensometerwidget.h"

TensometerWidget::TensometerWidget(QWidget* parent, QStringList* names)
    : ScreenWidget(parent)
{
    // create a subwidget like a counter
    QVBoxLayout* vLayout = new QVBoxLayout(this);
    this->setLayout(vLayout);

    QHBoxLayout* hLayout = new QHBoxLayout(NULL);
    vLayout->addLayout(hLayout);

    this->counter = new QLCDNumber(3, NULL);
    this->counter->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    hLayout->addWidget(counter);

    QLabel* unitLabel = new QLabel("Volt", NULL);
    unitLabel->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
    hLayout->addWidget(unitLabel);

    // okay, fix this one by adding the right signal. There is a bit of work to
    // do.
    connect(this->counter, SIGNAL(objectNameChanged(QString)), this,
            SLOT(click_handler(QEvent*)));

    QPushButton* menuButton = new QPushButton("Menu");
    vLayout->addWidget(menuButton);

    connect(menuButton, SIGNAL(clicked(bool)), this, SLOT(showMainMenu()));

    // add our name to the main menu buttons

    names->push_back(QString("Tension"));

}



void TensometerWidget::click_handler(QEvent *e)
{
    this->hide();
}
