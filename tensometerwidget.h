#ifndef TENSOMETERWIDGET_H
#define TENSOMETERWIDGET_H

#include "screenwidget.h"
#include <QLCDNumber>
#include <QEvent>
#include <QPushButton>
#include <QLabel>

class TensometerWidget : public ScreenWidget
{
    Q_OBJECT
private:
    QLCDNumber* counter;

public:
    TensometerWidget(QWidget* parent, QStringList *names);
public slots:
    void click_handler(QEvent *e);
};

#endif // SPEEDOMETERWIDGET_H
