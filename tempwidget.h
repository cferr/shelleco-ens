#ifndef TEMPWIDGET_H
#define TEMPWIDGET_H

#include "screenwidget.h"
#include <QLCDNumber>
#include <QEvent>
#include <QPushButton>
#include <QLabel>

class TempWidget : public ScreenWidget
{
    Q_OBJECT
private:
    QLCDNumber* counter;

public:
    TempWidget(QWidget* parent, QStringList *names);
public slots:
    void click_handler(QEvent *e);
};

#endif // SPEEDOMETERWIDGET_H
