#include "carmonwindow.h"

#include "speedometerwidget.h"
#include "tempwidget.h"
#include "tensometerwidget.h"

carmonWindow::carmonWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->names = new QStringList();
    this->stack = new QStackedWidget(this);
    this->setCentralWidget(stack);
    /* What this function should do : instantiate a copy of each module widget,
     * then show up the main menu. Meanwhile, a "Please wait" message may be
     * displayed to the user. Modules shouldn't take long to instantiate. */
    // okay, this call fucks up everything : the speedometer shows up even
    // before sp->show() is called !
    //sp->show();

    /* This has to be the first one to be added, until the menu reference
     * is stored somehow */
    MainMenu* m = new MainMenu(this);
    connect(m, SIGNAL(show(int)), this, SLOT(showModule(int)));
    stack->addWidget(m);


    SpeedometerWidget* w_sp = new SpeedometerWidget(this, this->names);
    stack->addWidget(w_sp);
    connect(w_sp, SIGNAL(mainMenu()), this, SLOT(showMainMenu()));

    TensometerWidget* w_tens = new TensometerWidget(this, this->names);
    stack->addWidget(w_tens);
    connect(w_tens, SIGNAL(mainMenu()), this, SLOT(showMainMenu()));

    TempWidget* w_temp = new TempWidget(this, this->names);
    stack->addWidget(w_temp);
    connect(w_temp, SIGNAL(mainMenu()), this, SLOT(showMainMenu()));


    m->update(this->names);
    stack->setCurrentIndex(0);
}

carmonWindow::~carmonWindow()
{
}

void carmonWindow::showMainMenu()
{
    this->stack->setCurrentIndex(0);
}

void carmonWindow::showModule(int n)
{
    this->stack->setCurrentIndex(n);
}
