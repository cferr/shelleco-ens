#ifndef SPEEDOMETERWIDGET_H
#define SPEEDOMETERWIDGET_H

#include "screenwidget.h"
#include <QLCDNumber>
#include <QEvent>
#include <QPushButton>
#include <QLabel>

class SpeedometerWidget : public ScreenWidget
{
    Q_OBJECT
private:
    QLCDNumber* counter;

public:
    SpeedometerWidget(QWidget* parent, QStringList *names);
public slots:
    void click_handler(QEvent *e);
};

#endif // SPEEDOMETERWIDGET_H
