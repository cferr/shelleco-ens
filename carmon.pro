#-------------------------------------------------
#
# Project created by QtCreator 2016-03-12T22:47:25
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = carmon
TEMPLATE = app


SOURCES += main.cpp\
        carmonwindow.cpp \
    screenwidget.cpp \
    speedometerwidget.cpp \
    mainmenu.cpp \
    tensometerwidget.cpp \
    tempwidget.cpp

HEADERS  += carmonwindow.h \
    screenwidget.h \
    speedometerwidget.h \
    mainmenu.h \
    tensometerwidget.h \
    tempwidget.h

FORMS    +=

DISTFILES += \
    DEVELOPER_INFO
