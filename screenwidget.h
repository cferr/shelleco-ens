#ifndef SCREENWIDGET_H
#define SCREENWIDGET_H
#include <QIcon>
#include <QString>
#include <QWidget>
#include <QVBoxLayout>

/**
 * @brief Base class for screen widgets.
 * Every autonomous module that is meant to have a menu item should extend
 * this class.
 * In CarmonWindow, there will be a vector of ScreenWidget's, which will
 * be used to refrence them all.
 */

// TODO : implement generic click handler signal for this widget, that
// catches clicks on widgets which have no click event handler.
// Why am I going to do this ? Simple answer : the Pi touchscreen !

class ScreenWidget : public QWidget
{
    Q_OBJECT
private:
    QIcon icon;
    QString title;
    // in a near future we'll have a controller class for this

protected slots:
    void showMainMenu() {
        emit mainMenu();
    }

public:
    explicit ScreenWidget(QWidget *parent = 0, QStringList* widgets = 0);
    QIcon& getIcon();
    QString getTitle();

signals:
    void mainMenu();

public slots:
};

#endif // SCREENWIDGET_H
