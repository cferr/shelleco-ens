#ifndef MAINMENU_H
#define MAINMENU_H
#include <vector>

#include <QWidget>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>

class MainMenu : public QWidget
{
    Q_OBJECT
private:
    std::vector<QPushButton*> buttons; // unsorted
    QHBoxLayout* top;
    QHBoxLayout* bottom;
    QVBoxLayout* container;

    QPushButton* topLeft;
    QPushButton* topRight;
    QPushButton* bottomLeft;
    QPushButton* bottomRight;


    int curN = 0;

public:
    MainMenu(QWidget* parent = 0);
    ~MainMenu();
    void update(QStringList* names); // the order of the items here matters.

signals:
    void show(int n);

private slots:
    void action1Triggered();
    void action2Triggered();
    void action3Triggered();
    void action4Triggered();


};

#endif // MAINMENU_H
