#include "mainmenu.h"

MainMenu::MainMenu(QWidget* parent) : QWidget(parent)
{
    this->container = new QVBoxLayout();
    this->top = new QHBoxLayout();
    this->bottom = new QHBoxLayout();

    topLeft = new QPushButton("(No add-on)");
    topRight = new QPushButton("(No add-on)");

    bottomLeft = new QPushButton("(No add-on)");
    bottomRight = new QPushButton("(No add-on)");

    topLeft->connect(topLeft, SIGNAL(clicked()), this, SLOT(action1Triggered()));
    topRight->connect(topRight, SIGNAL(clicked()), this, SLOT(action2Triggered()));
    bottomLeft->connect(bottomLeft, SIGNAL(clicked()), this, SLOT(action3Triggered()));
    bottomRight->connect(bottomRight, SIGNAL(clicked()), this, SLOT(action4Triggered()));

    topLeft->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    topRight->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    bottomLeft->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    bottomRight->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    top->addWidget(topLeft);
    top->addWidget(bottomLeft);
    bottom->addWidget(topRight);
    bottom->addWidget(bottomRight);

    container->addLayout(top);
    container->addLayout(bottom);

    this->setLayout(container);
}

MainMenu::~MainMenu()
{
    delete this->top;
    delete this->bottom;
}

void MainMenu::update(QStringList* names)
{
    int l = names->length();
    if(l > curN)
        topLeft->setText(names->at(curN));
    if(l > curN + 1)
        topRight->setText(names->at(curN + 1));
    if(l > curN + 2)
        bottomLeft->setText(names->at(curN + 2));
    if(l > curN + 3)
        bottomRight->setText(names->at(curN + 3));

}

void MainMenu::action1Triggered()
{
    // launch action 1
    emit show(curN + 1);
}

void MainMenu::action2Triggered()
{
    emit show(curN + 2);
}

void MainMenu::action3Triggered()
{
    emit show(curN + 3);
}

void MainMenu::action4Triggered()
{
    emit show(curN + 4);
}

