#ifndef CARMONWINDOW_H
#define CARMONWINDOW_H

#include <QMainWindow>
#include <QStackedWidget>
#include <QStringList>
#include "mainmenu.h"

namespace Ui {
    class carmonWindow;
}

class carmonWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit carmonWindow(QWidget *parent = 0);
    ~carmonWindow();
    QStringList* names;

private:
    QStackedWidget* stack;


private slots:
    void showMainMenu();
    void showModule(int n); // the n is the order of insertion
};

#endif // CARMONWINDOW_H
